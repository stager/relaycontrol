#include "arceeprom.h"
#include "arcserial.h"
#include "arcalarm.h"
#include "arcnet.h"
#include "arcrelay.h"

/************************************************************

  This sketch requires ethercard library.

  ECN28J60 ethernet module must be connected to Arduino Uno:

  Arduino     ECN28J60
    D10   ->    CS
    D11   ->    SI
    D12   ->    SO
    D13   ->    SCK
    3.3V  ->    VCC
    GND   ->    GND

  By default sketch uses dhcp and supports 8 relays connected
  to Arduino pins 2-9 in "inverted mode (activated by low
  level)

  To change these settings use arcctl application

*************************************************************/

#define ARCAlarmLedPin (13)

static ARCRelay::Config relayConfig[ARCRelayMaxCount];
static ARCNetConfig     netConfig;

ARCNet        net;
ARCSerialPort serial(&netConfig,relayConfig);

void setup()
 {
  serial.begin();

  if (ARCEEPROMCheckMagic())
   {
    ARCRelay::configReadEEPROM(relayConfig,ARCEEPROMGetDataOffset());
    ARCNet::configReadEEPROM(&netConfig,ARCEEPROMGetDataOffset() + ARCRelay::configGetEEPROMSize());
   }
  else
   {
    ARCEEPROMWriteMagic();

    ARCRelay::configSetToDefault(relayConfig);
    ARCRelay::configWriteEEPROM(relayConfig,ARCEEPROMGetDataOffset());

    ARCNet::configSetToDefault(&netConfig);
    ARCNet::configWriteEEPROM(&netConfig,ARCEEPROMGetDataOffset() + ARCRelay::configGetEEPROMSize());
   }

  alarm.begin(ARCAlarmLedPin);
  relay.begin(relayConfig);
  net.begin(&netConfig);
 }

void loop ()
 {
  alarm.update();
  serial.update();
  net.update();
  relay.update();
 }

