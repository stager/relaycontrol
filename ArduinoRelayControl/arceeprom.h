#ifndef __ARCEEPROM_H__
#define __ARCEEPROM_H__

#define ARC_FW_VER_MAJOR (0)
#define ARC_FW_VER_MINOR (2)

extern bool ARCEEPROMCheckMagic       ();
extern void ARCEEPROMWriteMagic       ();
extern int  ARCEEPROMGetDataOffset    ();

#endif

