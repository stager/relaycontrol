#ifndef __ARCNET_H__
#define __ARCNET_H__

#include <EtherCard.h>

#define MACAddressLen (6)
#define IPAddressLen  (4)

typedef struct
 {
  byte   ip[IPAddressLen];
  byte   gw[IPAddressLen];
  byte   mac[MACAddressLen];
  bool   dhcp;
 } ARCNetConfig;

class ARCNet
 {
  public           :

                   ARCNet                  ();
  void             begin                   (ARCNetConfig       *config);
  void             update                  ();

  static void      configSetToDefault      (ARCNetConfig       *config);
  static void      configReadEEPROM        (ARCNetConfig       *config,
                                            int                 addr);
  static void      configWriteEEPROM       (const ARCNetConfig *config,
                                            int                 addr);
  static int       configGetEEPROMSize     ();

  private          :

  bool             active;

  void             processActivateRequest  (const char         *args);
  void             processDeActivateRequest(const char         *args);
  void             processStatusRequest    ();

  void             sendRelaySuccessReply   ();
  void             sendRelayErrorReply     ();
  void             sendArgErrorReply       ();

  void             sendNotFoundReply       ();
 };

#endif

