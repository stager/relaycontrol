#include <EEPROM.h>

#include "arcalarm.h"
#include "arcrelay.h"
#include "arcnet.h"

#define ARCNetEthernetBufferSize (500)

byte Ethernet::buffer[ARCNetEthernetBufferSize];

static const byte ARCNetDefaultMAC[] PROGMEM =
 { 0x74,0x69,0x69,0x2D,0x30,0x31 };

static const char ARCNetTextPagePrefix[] PROGMEM =
"HTTP/1.0 200 OK\r\n"
"Content-Type: text/plain\r\n"
"\r\n";

static const char ARCNetRelaySuccessPage[] PROGMEM =
"HTTP/1.0 200 OK\r\n"
"Content-Type: text/html\r\n"
"\r\n"
"<html>"
"<head><title>Success</title></head>"
"<body>"
"Ok"
"</body>"
"<html>";

static const char ARCNetArgErrorPage[] PROGMEM =
"HTTP/1.0 400 OK\r\n"
"Content-Type: text/html\r\n"
"\r\n"
"<html>"
"<head><title>Error</title></head>"
"<body>"
"Invalid argument"
"</body>"
"<html>";

static const char ARCNetNotFoundPage[] PROGMEM =
"HTTP/1.0 404 Not found\r\n"
"Content-Type: text/html\r\n"
"\r\n"
"<html>"
"<head><title>Page not found</title></head>"
"<body>"
"Not found"
"</body>"
"<html>";

static const char *ARCNetSkipGetPrefix (const char *request)
 {
  if (request[0] == 'G' &&
      request[1] == 'E' &&
      request[2] == 'T' &&
      request[3] == ' ' &&
      request[4] == '/')
   {
    return &request[5];
   }
  else
   {
    return NULL;
   }
 }

static const char *ARCNetSkipReqName (const char *request,
                                      const char *name)
 {
  while (*request == *name && *name != '\0')
   {
    request++;
    name++;
   }

  return *name == '\0' ? request : NULL;
 }

static bool ARCNetScanUInt (unsigned int *value,
                            const char  **str)
 {
  if (!isDigit(**str))
   {
    return false;
   }

  *value = 0;

  while (isDigit(**str))
   {
    (*value) *= 10;

    (*value) += (**str) - '0';

    (*str)++;
   }

  return value;
 }

static const char ARCNetGetStatusPrefix[] = "GET /status";

ARCNet::ARCNet                        ()
 {
  active = false;
 }

void
ARCNet::begin                         (ARCNetConfig       *config)
 {
  if (ether.begin(sizeof(Ethernet::buffer),config->mac) == 0)
   {
    alarm.etherHwError();

    return;
   }

  if (config->dhcp)
   {
    if (!ether.dhcpSetup())
     {
      alarm.etherCfgError();

      return;
     }
   }
  else
   {
    ether.staticSetup(config->ip,config->gw);
   }

  active = true;
 }

void
ARCNet::update                        ()
 {
  if (!active)
   {
    return;
   }

  word len = ether.packetReceive();
  word pos = ether.packetLoop(len);

  if (pos == 0) return;

  const char *request = ARCNetSkipGetPrefix((char*)(Ethernet::buffer + pos));

  if (request != NULL)
   {
    const char *args;

    if      ((args = ARCNetSkipReqName(request,"activate?r=")) != NULL)
     {
      processActivateRequest(args);
     }
    else if ((args = ARCNetSkipReqName(request,"deactivate?r=")) != NULL)
     {
      processDeActivateRequest(args);
     }
    else if ((args = ARCNetSkipReqName(request,"status")) != NULL)
     {
      processStatusRequest();
     }
    else
     {
      sendNotFoundReply();
     }
   }
  else
   {
    sendNotFoundReply();
   }
 }

void
ARCNet::processActivateRequest        (const char         *args)
 {
  unsigned int relayIndex;
  unsigned int duration = 0;

  bool ok = ARCNetScanUInt(&relayIndex,&args);

  if (ok)
   {
    if ((*args++) == ',')
     {
      ok = ARCNetScanUInt(&duration,&args);
     }
   }

  if (ok)
   {
    if (relay.activate(relayIndex,duration))
     {
      sendRelaySuccessReply();
     }
    else
     {
      sendRelayErrorReply();
     }
   }
  else
   {
    sendArgErrorReply();
   }
 }

void
ARCNet::processDeActivateRequest      (const char         *args)
 {
  unsigned int relayIndex;

  bool ok = ARCNetScanUInt(&relayIndex,&args);

  if (ok)
   {
    if (relay.deactivate(relayIndex))
     {
      sendRelaySuccessReply();
     }
    else
     {
      sendRelayErrorReply();
     }
   }
  else
   {
    sendArgErrorReply();
   }
 }

void
ARCNet::processStatusRequest          ()
 {
  sendNotFoundReply();
 }

void
ARCNet::sendRelaySuccessReply         ()
 {
  memcpy_P(ether.tcpOffset(),ARCNetRelaySuccessPage,sizeof(ARCNetRelaySuccessPage));

  ether.httpServerReply(sizeof(ARCNetRelaySuccessPage) - 1);
 }

void
ARCNet::sendRelayErrorReply           ()
 {
  sendArgErrorReply();
 }

void
ARCNet::sendArgErrorReply             ()
 {
  memcpy_P(ether.tcpOffset(),ARCNetArgErrorPage,sizeof(ARCNetArgErrorPage));

  ether.httpServerReply(sizeof(ARCNetArgErrorPage) - 1);
 }

void
ARCNet::sendNotFoundReply             ()
 {
  memcpy_P(ether.tcpOffset(),ARCNetNotFoundPage,sizeof(ARCNetNotFoundPage));

  ether.httpServerReply(sizeof(ARCNetNotFoundPage) - 1);
 }

void
ARCNet::configSetToDefault            (ARCNetConfig       *config)
 {
  for (int i = 0; i < IPAddressLen; i++)
   {
    config->ip[i] = 0;
    config->gw[i] = 0;
   }

  memcpy_P(config->mac,ARCNetDefaultMAC,sizeof(ARCNetDefaultMAC));

  config->dhcp = true;
 }

void
ARCNet::configReadEEPROM              (ARCNetConfig       *config,
                                       int                 addr)
 {
  for (int i = 0; i < IPAddressLen; i++)
   {
    config->ip[i] = EEPROM.read(addr++);
   }

  for (int i = 0; i < IPAddressLen; i++)
   {
    config->gw[i] = EEPROM.read(addr++);
   }

  for (int i = 0; i < MACAddressLen; i++)
   {
    config->mac[i] = EEPROM.read(addr++);
   }

  config->dhcp = EEPROM.read(addr);
 }

void
ARCNet::configWriteEEPROM             (const ARCNetConfig *config,
                                       int                 addr)
 {
  for (int i = 0; i < IPAddressLen; i++)
   {
    EEPROM.update(addr++,config->ip[i]);
   }

  for (int i = 0; i < IPAddressLen; i++)
   {
    EEPROM.update(addr++,config->gw[i]);
   }

  for (int i = 0; i < MACAddressLen; i++)
   {
    EEPROM.update(addr++,config->mac[i]);
   }

  EEPROM.update(addr,config->dhcp);
 }

int
ARCNet::configGetEEPROMSize           ()
 {
  return 2 * IPAddressLen + MACAddressLen + 1;
 }

