#ifndef __ARCSERIAL_H__
#define __ARCSERIAL_H__

#include "arcnet.h"
#include "arcrelay.h"

#define ARCSerialSpeed (57600)

class ARCSerialPort
 {
  public           :

                   ARCSerialPort      (ARCNetConfig       *netConfig,
                                       ARCRelay::Config   *relayConfig);

  void             begin              ();
  void             update             ();

  private          :

  void             processChar        (int                 value);
  void             processCommand     ();
  void             processCmdVersion  ();
  void             processCmdInfo     ();
  void             processCmdSetRelay (const char         *args);
  void             processCmdSetMAC   (const char         *args);
  void             processCmdSetIpDHCP();
  void             processCmdSetIpStatic();
  void             processCmdWriteEEPROM();
  void             processCmdSetIpAddr(const char         *args,
                                       int                 ipType);

  void             sendError          (int                 errorCode);
  void             sendOk             ();

  enum { BUFFER_SIZE = 128 };

  enum { ERR_INVALID_CMD, ERR_INVALID_ARGS, ERR_CMD_TOO_LARGE };

  unsigned int     pos;
  char             buffer[BUFFER_SIZE];

  ARCNetConfig    *netConfig;
  ARCRelay::Config*relayConfig;
 };

#endif

