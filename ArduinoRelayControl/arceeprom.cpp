#include <Arduino.h>
#include <EEPROM.h>

#include "arceeprom.h"

static char ARCEEPROMMagic[] = { 'A', 'R', 'C', 'z', 'z', 'z', ARC_FW_VER_MAJOR, ARC_FW_VER_MINOR };

bool ARCEEPROMCheckMagic       ()
 {
  for (int i = 0; i < sizeof(ARCEEPROMMagic); i++)
   {
    if (EEPROM.read(i) != ARCEEPROMMagic[i])
     {
      return false;
     }
   }

  return true;
 }

void ARCEEPROMWriteMagic       ()
 {
  for (int i = 0; i < sizeof(ARCEEPROMMagic); i++)
   {
    EEPROM.update(i,ARCEEPROMMagic[i]);
   }
 }

int ARCEEPROMGetDataOffset    ()
 {
  return sizeof(ARCEEPROMMagic);
 }

