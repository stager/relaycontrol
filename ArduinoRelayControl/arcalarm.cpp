#include <Arduino.h>

#include "arcalarm.h"

static unsigned int ARCAlarmHwErrorSequence[]  = { 250,   250 };
static unsigned int ARCAlarmCfgErrorSequence[] = { 1000, 1000 };

ARCAlarm alarm;

ARCAlarm::ARCAlarm                    ()
 {
 }

void
ARCAlarm::begin                       (unsigned char      alarmPin)
 {
  pin = alarmPin;

  pinMode(pin,OUTPUT);

  disableAlarm();
 }

void
ARCAlarm::update                      ()
 {
  if (sequence == NULL)
   {
    return;
   }

  unsigned long currTime = millis();

  if (currTime - startTime >= sequence[sequencePos])
   {
    sequencePos++;

    if (sequencePos >= sequenceLen)
     {
      sequencePos = 0;
     }

    startTime = currTime;

    if (sequencePos % 2 == 0)
     {
      digitalWrite(pin,HIGH);
     }
    else
     {
      digitalWrite(pin,LOW);
     }
   }
 }

void
ARCAlarm::disableAlarm                ()
 {
  sequence    = NULL;
  startTime   = 0;
  sequencePos = 0;
  sequenceLen = 0;
 }

void
ARCAlarm::etherHwError                ()
 {
  sequence    = ARCAlarmHwErrorSequence;
  sequencePos = 0;
  sequenceLen = sizeof(ARCAlarmHwErrorSequence) / sizeof(ARCAlarmHwErrorSequence[0]);
  startTime   = millis();

  digitalWrite(pin,HIGH);
 }

void
ARCAlarm::etherCfgError               ()
 {
  sequence    = ARCAlarmCfgErrorSequence;
  sequencePos = 0;
  sequenceLen = sizeof(ARCAlarmCfgErrorSequence) / sizeof(ARCAlarmCfgErrorSequence[0]);
  startTime   = millis();

  digitalWrite(pin,HIGH);
 }

