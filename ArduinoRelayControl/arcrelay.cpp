#include <Arduino.h>
#include <EEPROM.h>

#include "arcrelay.h"

#define ARCRelayStartPin (2)
#define DISABLED_PIN (0xFF)

ARCRelay relay;

ARCRelay::ARCRelay                    ()
 {
  init();

  config = NULL;
 }

void
ARCRelay::begin                       (const Config       *config)
 {
  init();

  this->config = config;

  for (int i = 0; i < ARCRelayMaxCount; i++)
   {
    if (config[i].pin != DISABLED_PIN)
     {
      pinMode(config[i].pin,OUTPUT);
      digitalWrite(config[i].pin,config[i].inverted ? HIGH : LOW);
     }
   }
 }

void
ARCRelay::init                        ()
 {
  for (int i = 0; i < ARCRelayMaxCount; i++)
   {
    relays[i].active    = false;
    relays[i].startTime = 0;
    relays[i].duration  = 0;
   }
 }

void
ARCRelay::update                      ()
 {
  unsigned long currTime = millis();

  for (int i = 0; i < ARCRelayMaxCount; i++)
   {
    if (relays[i].active && relays[i].duration > 0)
     {
      if (currTime - relays[i].startTime >= relays[i].duration)
       {
        deactivate(i);
       }
     }
   }
 }

bool
ARCRelay::activate                    (unsigned int        relayIndex,
                                       unsigned long       duration)
 {
  if (relayIndex >= ARCRelayMaxCount)
   {
    return false;
   }

  if (config[relayIndex].pin == DISABLED_PIN)
   {
    return false;
   }

  relays[relayIndex].active    = true;
  relays[relayIndex].startTime = millis();
  relays[relayIndex].duration  = duration;

  digitalWrite(config[relayIndex].pin,config[relayIndex].inverted ? LOW : HIGH);

  return true;
 }

bool
ARCRelay::deactivate                  (unsigned int        relayIndex)
 {
  if (relayIndex >= ARCRelayMaxCount)
   {
    return false;
   }

  if (config[relayIndex].pin == DISABLED_PIN)
   {
    return false;
   }

  relays[relayIndex].active    = false;
  relays[relayIndex].startTime = 0;
  relays[relayIndex].duration  = 0;

  digitalWrite(config[relayIndex].pin,config[relayIndex].inverted ? HIGH : LOW);

  return true;
 }

void
ARCRelay::configSetToDefault          (Config             *config)
 {
  for (int i = 0; i < ARCRelayMaxCount; i++)
   {
    config[i].pin      = ARCRelayStartPin + i;
    config[i].inverted = true;
   }
 }

void
ARCRelay::configReadEEPROM            (Config             *config,
                                       int                 addr)
 {
  for (int i = 0; i < ARCRelayMaxCount; i++)
   {
    config[i].pin      = EEPROM.read(addr + i * 2);
    config[i].inverted = EEPROM.read(addr + i * 2 + 1);
   }
 }

void
ARCRelay::configWriteEEPROM           (const Config       *config,
                                       int                 addr)
 {
  for (int i = 0; i < ARCRelayMaxCount; i++)
   {
    EEPROM.update(addr + i * 2,config[i].pin);
    EEPROM.update(addr + i * 2 + 1,config[i].inverted);
   }
 }

int
ARCRelay::configGetEEPROMSize         ()
 {
  return ARCRelayMaxCount * 2;
 }

