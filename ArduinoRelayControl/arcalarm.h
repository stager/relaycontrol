#ifndef __ARCALARM_H__
#define __ARCALARM_H__

class ARCAlarm
 {
  public           :

                   ARCAlarm           ();

  void             begin              (unsigned char      alarmPin);
  void             update             ();

  void             disableAlarm       ();

  void             etherHwError       ();
  void             etherCfgError      ();

  private          :

  unsigned int    *sequence;
  unsigned long    startTime;
  unsigned char    sequencePos;
  unsigned char    sequenceLen;
  unsigned char    pin;
 };

extern ARCAlarm alarm;

#endif

