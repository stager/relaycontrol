#include <Arduino.h>

#include "arceeprom.h"
#include "arcserial.h"

#define IP_TYPE_IP (0)
#define IP_TYPE_GW (1)

static const char *str_starts_with (const char *s, const char *prefix)
 {
  while (*s == *prefix && *prefix != '\0')
   {
    s++; prefix++;
   }

  if (*prefix == '\0')
   {
    return s;
   }
  else
   {
    return NULL;
   }
 }

static const char *str_parse_byte_dec (unsigned char *value, const char *str)
 {
  unsigned int v = 0;
  const char  *s = str;

  while (*s >= '0' && *s <= '9')
   {
    v *= 10;
    v += (*s) - '0';

    s++;
   }

  if (s == str || v > 255)
   {
    return NULL;
   }
  else
   {
    *value = v;

    return s;
   }
 }

static bool str_parse_byte_list_dec   (unsigned char      *dest,
                                       unsigned int        count,
                                       const char         *str)
 {
  bool ok = true;

  for (unsigned int i = 0; ok && i < count; i++)
   {
    str = str_parse_byte_dec(&dest[i],str);

    if (str != NULL)
     {
      if (i + 1 < count)
       {
        if (*str == ',')
         {
          str++;
         }
        else
         {
          ok = false;
         }
       }
     }
    else
     {
      ok = false;
     }
   }

  return ok;
 }

static bool str_parse_mac             (unsigned char      *mac,
                                       const char         *str)
 {
  return str_parse_byte_list_dec(mac,MACAddressLen,str);
 }

static bool str_parse_ip              (unsigned char      *ip,
                                       const char         *str)
 {
  return str_parse_byte_list_dec(ip,IPAddressLen,str);
 }

ARCSerialPort::ARCSerialPort          (ARCNetConfig       *netConfig,
                                       ARCRelay::Config   *relayConfig)
 {
  this->netConfig   = netConfig;
  this->relayConfig = relayConfig;
 }

void ARCSerialPort::begin             ()
 {
  Serial.begin(ARCSerialSpeed);

  pos = 0;
 }

void ARCSerialPort::update            ()
 {
  while (Serial.available())
   {
    processChar(Serial.read());
   }
 }

void ARCSerialPort::sendError         (int                 errorCode)
 {
  Serial.print("ERROR ");
  Serial.println(errorCode);
 }

void ARCSerialPort::sendOk            ()
 {
  Serial.println("OK");
 }

void ARCSerialPort::processCmdVersion ()
 {
  sendOk();

  Serial.print(ARC_FW_VER_MAJOR);
  Serial.print(".");
  Serial.print(ARC_FW_VER_MINOR);
  Serial.println();
 }

static void SerialPrintIp (unsigned char *ip)
 {
  for (int i = 0; i < IPAddressLen; i++)
   {
    Serial.print(ip[i]);

    if (i + 1 < IPAddressLen)
     {
      Serial.print(".");
     }
   }
 }

void ARCSerialPort::processCmdInfo ()
 {
  sendOk();

  Serial.print("FW: ");
  Serial.print(ARC_FW_VER_MAJOR);
  Serial.print(".");
  Serial.print(ARC_FW_VER_MINOR);
  Serial.println();

  Serial.print("MAC: ");
  for (int i = 0; i < MACAddressLen; i++)
   {
    Serial.print(netConfig->mac[i],HEX);

    if (i + 1 < MACAddressLen)
     {
      Serial.print(":");
     }
   }

  Serial.println();

  Serial.print("IPMODE: ");

  if (netConfig->dhcp)
   {
    Serial.println("dhcp");
   }
  else
   {
    Serial.println("static");
   }

  Serial.print("IP: ");
  SerialPrintIp(netConfig->ip);
  Serial.println();

  Serial.print("GW: ");
  SerialPrintIp(netConfig->gw);
  Serial.println();

  for (int i = 0; i < ARCRelayMaxCount; i++)
   {
    Serial.print("Relay: ");
    Serial.print(i);
    Serial.print(" pin ");
    Serial.print(relayConfig[i].pin);

    if (relayConfig[i].inverted)
     {
      Serial.print(" inverted");
     }

    Serial.println();
   }

  sendOk();
 }

void ARCSerialPort::processCmdSetRelay(const char         *args)
 {
  unsigned char setup[3];

  if (str_parse_byte_list_dec(setup,3,args))
   {
    if (setup[0] < ARCRelayMaxCount && setup[2] < 2)
     {
      relayConfig[setup[0]].pin      = setup[1];
      relayConfig[setup[0]].inverted = setup[2] == 0;

      sendOk();
     }
    else
     {
      sendError(ERR_INVALID_ARGS);
     }
   }
  else
   {
    sendError(ERR_INVALID_ARGS);
   }
 }

void ARCSerialPort::processCmdSetMAC  (const char         *args)
 {
  unsigned char mac[MACAddressLen];

  if (str_parse_mac(mac,args))
   {
    for (int i = 0; i < MACAddressLen; i++)
     {
      netConfig->mac[i] = mac[i];
     }

    sendOk();
   }
  else
   {
    sendError(ERR_INVALID_ARGS);
   }
 }

void ARCSerialPort::processCmdSetIpAddr(const char         *args,
                                        int                 ipType)
 {
  unsigned char ip[IPAddressLen];

  if (str_parse_ip(ip,args))
   {
    for (int i = 0; i < IPAddressLen; i++)
     {
      if (ipType == IP_TYPE_IP)
       {
        netConfig->ip[i] = ip[i];
       }
      else
       {
        netConfig->gw[i] = ip[i];
       }
     }

    sendOk();
   }
  else
   {
    sendError(ERR_INVALID_ARGS);
   }
 }

void ARCSerialPort::processCmdSetIpDHCP ()
 {
  netConfig->dhcp = true;

  sendOk();
 }

void ARCSerialPort::processCmdSetIpStatic ()
 {
  netConfig->dhcp = false;

  sendOk();
 }

void ARCSerialPort::processCmdWriteEEPROM ()
 {
  ARCRelay::configWriteEEPROM(relayConfig,ARCEEPROMGetDataOffset());
  ARCNet::configWriteEEPROM(netConfig,ARCEEPROMGetDataOffset() + ARCRelay::configGetEEPROMSize());

  sendOk();
 }

void ARCSerialPort::processCommand    ()
 {
  const char *args;

  if      (str_starts_with(buffer,"VER\r"))
   {
    processCmdVersion();
   }
  else if (str_starts_with(buffer,"INFO\r"))
   {
    processCmdInfo();
   }
  else if ((args = str_starts_with(buffer,"SETMAC ")) != NULL)
   {
    processCmdSetMAC(args);
   }
  else if (str_starts_with(buffer,"SETIPDHCP\r"))
   {
    processCmdSetIpDHCP();
   }
  else if (str_starts_with(buffer,"SETIPSTATIC\r"))
   {
    processCmdSetIpStatic();
   }
  else if ((args = str_starts_with(buffer,"SETIPIP ")) != NULL)
   {
    processCmdSetIpAddr(args,IP_TYPE_IP);
   }
  else if ((args = str_starts_with(buffer,"SETIPGW ")) != NULL)
   {
    processCmdSetIpAddr(args,IP_TYPE_GW);
   }
  else if ((args = str_starts_with(buffer,"SETRELAY ")) != NULL)
   {
    processCmdSetRelay(args);
   }
  else if (str_starts_with(buffer,"WRITEEEPROM\r"))
   {
    processCmdWriteEEPROM();
   }
  else
   {
    sendError(ERR_INVALID_CMD);
   }
 }

void ARCSerialPort::processChar       (int value)
 {
  if (value == '\n')
   {
    buffer[pos] = '\0';

    processCommand();

    pos = 0;
   }
  else
   {
    buffer[pos++] = value;

    if (pos >= BUFFER_SIZE)
     {
      // buffer is filled, no space for trailing '\0'

      pos = 0;

      sendError(ERR_CMD_TOO_LARGE);
     }
   }
 }

