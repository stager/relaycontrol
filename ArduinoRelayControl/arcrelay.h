#ifndef __ARCRELAY_H__
#define __ARCRELAY_H__

#include <Arduino.h>

#define ARCRelayMaxCount (8)

class ARCRelay
 {
  public           :

  typedef struct
   {
    byte  pin;
    bool  inverted;
   } Config;

  static void      configSetToDefault (Config             *config);
  static void      configReadEEPROM   (Config             *config,
                                       int                 addr);
  static void      configWriteEEPROM  (const Config       *config,
                                       int                 addr);
  static int       configGetEEPROMSize();

                   ARCRelay           ();

  void             begin              (const Config       *config);
  void             update             ();

  bool             activate           (unsigned int        relayIndex,
                                       unsigned long       duration = 0);

  bool             deactivate         (unsigned int        relayIndex);

  private          :

  void             init               ();

  typedef struct
   {
    unsigned long startTime;
    unsigned long duration;
    bool          active;
   } Status;

  Status           relays[ARCRelayMaxCount];
  const Config    *config;
 };

extern ARCRelay relay;

#endif

